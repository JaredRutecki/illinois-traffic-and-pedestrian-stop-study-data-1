Description [from IDOT website](https://idot.illinois.gov/transportation-system/local-transportation-partners/law-enforcement/illinois-traffic-stop-study):

On July 18, 2003, Senate Bill 30 was signed into law to establish a four-year statewide study of data from traffic stops to identify racial bias. The study began on January 1, 2004 and was originally scheduled to end December 31, 2007. However, the legislature extended the data collection several times, and also expanded the study to include data on pedestrian stops. Public Act 101-0024, which took effect on June 21, 2019, eliminated the study's scheduled end date of July 1, 2019 and extended the data collection.

IDOT is responsible for collecting and compiling the data. IDOT has developed an Internet self-reporting application for submission of the data. Agencies have two options for entering data into the IDOT data collection system.

- The agency can use the IDOT web site to enter data on individual stops.
- The agency will have the option to compile the data using their own data application. The data can be exported from this application into a text file that can then be uploaded to IDOT's website.

Agencies using their own applications are encouraged to upload their data to IDOT at least once a month.


Forest is currently working on an easier way to work with these files. For now, they are tilde-delimited text files broken up by year, and in some cases, by geography and stop type (pedestrian vs. motor vehicle).
